import axios from 'axios';
import {Api} from '../../config/Api';
import RNFetchBlob from 'rn-fetch-blob';
import AsyncStorage from '@react-native-async-storage/async-storage/';

export const savePassenger = (payload, token) => {
  const image = payload.sign.split('data:image/png;base64,');

  return dispatch => {
    dispatch({
      type: 'SET_PASSENGER_LOADING',
      payload: true,
    });
    console.log('file');
    RNFetchBlob.fetch(
      'POST',
      `${Api}/passenger/store`,
      {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'multipart/form-data',
        Accept: 'application/json',
      },
      [
        {name: 'no_paspor', data: payload.no_paspor.toString()},
        {
          name: 'sign',
          filename: 'sign.png',
          data: image.toString(),
        },
        {name: 'bagasi_dibawa', data: payload.bagasi_dibawa.toString()},
        {
          name: 'bagasi_tdk_bersama',
          data: payload.bagasi_tdk_bersama.toString(),
        },
        {
          name: 'anggota_keluarga',
          data: payload.anggota_keluarga.toString(),
        },
        {name: 'no_penerbangan', data: payload.no_penerbangan.toString()},
        {name: 'f_11_a', data: payload.f_11_a.toString()},
        {name: 'f_11_b', data: payload.f_11_b.toString()},
        {name: 'f_11_c', data: payload.f_11_c.toString()},
        {name: 'f_11_d', data: payload.f_11_d.toString()},
        {name: 'f_11_e', data: payload.f_11_e.toString()},
        {name: 'f_11_f', data: payload.f_11_f.toString()},
        {name: 'f_11_g', data: payload.f_11_g.toString()},
        {name: 'f_11_a_ket', data: payload.f_11_a_ket.toString()},
        {name: 'f_11_b_ket', data: payload.f_11_b_ket.toString()},
        {name: 'f_11_c_ket', data: payload.f_11_c_ket.toString()},
        {name: 'f_11_d_ket', data: payload.f_11_d_ket.toString()},
        {name: 'f_11_e_ket', data: payload.f_11_e_ket.toString()},
        {name: 'f_11_f_ket', data: payload.f_11_f_ket.toString()},
        {name: 'f_11_g_ket', data: payload.f_11_g_ket.toString()},
        {name: 'transit', data: payload.transit.toString()},
        {name: 'transit_ket', data: payload.transit_ket.toString()},
      ],
    )
      // .then(response => {
      //   console.log(response.status);
      //   if (response.status === 401) {
      //     console.log('Logout Call from Sign (Passenger Create)');
      //     dispatch({
      //       type: 'REMOVE_TOKEN',
      //     });
      //     dispatch({
      //       type: 'SET_PASSENGER_LOADING',
      //       payload: false,
      //     });
      //     return;
      //   }
      // })
      .then(response => response.json())
      .then(datas => {
        // console.log(datas.message);

        if (datas.message === 'Unauthenticated.') {
          console.log('Logout Call from Sign (Passenger Create)');
          AsyncStorage.removeItem('user_data')
            .then(() => {
              dispatch({
                type: 'REMOVE_TOKEN',
              });
            })
            .catch(err => console.log(err));
        }

        if (datas.success) {
          dispatch({
            type: 'SET_PASSENGER_SUCCESS',
            payload:
              'Terima Kasih, Customs Declaration anda telah masuk ke sistem kami.',
          });
          dispatch({
            type: 'SET_PASSENGER_ERROR',
            payload: null,
          });
        } else {
          dispatch({
            type: 'SET_PASSENGER_ERROR',
            payload: 'Something Error ',
          });
          dispatch({
            type: 'SET_PASSENGER_SUCCESS',
            payload: null,
          });
        }

        dispatch({
          type: 'SET_PASSENGER_DONE',
          payload: true,
        });
        dispatch({
          type: 'SET_PASSENGER_LOADING',
          payload: false,
        });
      })
      .catch(err => {
        if (err.response?.data?.errors) {
          dispatch({
            type: 'SET_PASSENGER_ERROR',
            payload:
              'Something Error ' + JSON.stringify(err.response.data.errors),
          });
          dispatch({
            type: 'SET_PASSENGER_DONE',
            payload: true,
          });
          return;
        }
        dispatch({
          type: 'SET_PASSENGER_ERROR',
          payload: 'Something Error ',
        });
        dispatch({
          type: 'SET_PASSENGER_SUCCESS',
          payload: null,
        });
        console.log(err);
        // console.log(err.response?.data);

        dispatch({
          type: 'SET_PASSENGER_DONE',
          payload: true,
        });
        dispatch({
          type: 'SET_PASSENGER_LOADING',
          payload: false,
        });
      });
  };
};

export const deletePassenger = (id, token) => {
  return dispatch => {
    dispatch({
      type: 'SET_PASSENGER_LOADING',
      payload: true,
    });

    axios
      .delete(`${Api}/passenger/delete/${id}`, {
        headers: {Authorization: 'Bearer ' + token},
      })
      .then(result => {
        console.log(result.data);
        dispatch({
          type: 'SET_PASSENGER_LOADING',
          payload: false,
        });
      })
      .catch(err => {
        dispatch({
          type: 'SET_PASSENGER_LOADING',
          payload: false,
        });
        console.log(err);
      });
  };
};
