/* eslint-disable handle-callback-err */
// import AsyncStorage from '@react-native-async-storage/async-storage/';
import axios from 'axios';
import {Api} from '../../config/Api';

export const saveRegister = payload => {
  return dispatch => {
    axios
      .post(`${Api}/register`, payload, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(response => {
        console.log(response.data);
        dispatch({
          type: 'SET_REGISTER_LOADING',
          payload: false,
        });
        dispatch({
          type: 'SET_REGISTER_ERROR',
          payload: null,
        });
        dispatch({
          type: 'SET_REGISTER_SUCCESS',
          payload: 'Registrasi berhasil, silahkan login.',
        });
      })
      .catch(err => {
        dispatch({
          type: 'SET_REGISTER_LOADING',
          payload: false,
        });
        if (err.response?.data?.errors?.email) {
          dispatch({
            type: 'SET_REGISTER_ERROR',
            // payload: 'ada eror',
            payload: err.response.data.errors.email[0],
          });
          console.log(err.response.data);

          return;
        }
        dispatch({
          type: 'SET_REGISTER_ERROR',
          payload: 'Something Error',
        });
        dispatch({
          type: 'SET_REGISTER_SUCCESS',
          payload: null,
        });
        console.log(err);
      });
  };
};

export const getRegister = id => {
  return dispatch => {
    console.log(id);
  };
};
