/* eslint-disable handle-callback-err */
import AsyncStorage from '@react-native-async-storage/async-storage/';
import axios from 'axios';
import {Api} from '../../config/Api';

export const setAuth = params => {
  return dispatch => {
    dispatch({
      type: 'SET_LOADING',
      payload: true,
    });
    axios
      .post(`${Api}/login`, params)
      .then(data => {
        dispatch({type: 'SET_ERROR', payload: 'Email / Password salah.'});
        const jsonValue = JSON.stringify(data.data.user_data);
        AsyncStorage.setItem('user_data', jsonValue)
          .then(() => {
            dispatch({
              type: 'SET_TOKEN',
              payload: data.data.user_data,
            });
            dispatch({
              type: 'SET_LOADING',
              payload: false,
            });
          })
          .catch(err => console.log(err));
      })
      .catch(err => {
        dispatch({
          type: 'SET_LOADING',
          payload: false,
        });
        dispatch({type: 'SET_ERROR', payload: 'Email / Password salah.'});
      });
  };
};

export const getAuth = () => {
  return dispatch => {
    dispatch({
      type: 'SET_LOADING',
      payload: true,
    });
    AsyncStorage.getItem('user_data')
      .then(data => {
        console.log(data);
        dispatch({
          type: 'SET_TOKEN',
          payload: JSON.parse(data),
        });
        dispatch({
          type: 'SET_LOADING',
          payload: false,
        });
      })
      .catch(err => {
        dispatch({
          type: 'SET_LOADING',
          payload: false,
        });
        // console.log(err);
      });
  };
};

export const getLogout = token => {
  return dispatch => {
    dispatch({
      type: 'SET_LOADING',
      payload: true,
    });

    AsyncStorage.removeItem('user_data')
      .then(() => {
        dispatch({
          type: 'REMOVE_TOKEN',
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${Api}/logoutall`, {
        headers: {
          Accept: 'aplication/json',
          Authorization: 'Bearer ' + token,
        },
      })
      .then(result => {
        dispatch({
          type: 'SET_LOADING',
          payload: false,
        });
      })
      .catch(err => {
        console.log(err);
        dispatch({
          type: 'SET_LOADING',
          payload: false,
        });
      });
  };
};
