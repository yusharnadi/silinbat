import {setAuth, getAuth, getLogout} from './Auth';
import {saveRegister} from './Register';
import {savePassanger, deletePassenger} from './Passenger';

export {
  setAuth,
  getAuth,
  getLogout,
  saveRegister,
  savePassanger,
  deletePassenger,
};
