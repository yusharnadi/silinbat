import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';
import {AuthReducer, PassengerReducer, RegisterReducer} from './reducers';

const Reducers = combineReducers({
  AuthReducer,
  PassengerReducer,
  RegisterReducer,
});
const store = createStore(Reducers, applyMiddleware(thunk));
export default store;
