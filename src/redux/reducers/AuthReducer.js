const initialState = {
  token: null,
  name: null,
  no_paspor: null,
  id: null,
  loading: false,
  error: null,
};
export const AuthReducer = (state = initialState, action) => {
  if (action.type === 'SET_TOKEN') {
    return {
      ...state,
      token: action.payload.access_token,
      name: action.payload.name,
      no_paspor: action.payload.no_paspor,
      id: action.payload.id,
    };
  }

  if (action.type === 'REMOVE_TOKEN') {
    return {
      token: null,
      name: null,
      no_paspor: null,
      id: null,
    };
  }

  if (action.type === 'SET_LOADING') {
    return {
      ...state,
      loading: action.payload,
    };
  }

  if (action.type === 'SET_ERROR') {
    return {
      ...state,
      error: action.payload,
    };
  }

  return state;
};
