const initialState = {
  no_paspor: null,
  // kedatangan: '',
  bagasi_dibawa: 0,
  bagasi_tdk_bersama: 0,
  anggota_keluarga: 0,
  no_penerbangan: 0,
  f_11_a: null,
  f_11_a_ket: null,
  f_11_b: null,
  f_11_b_ket: null,
  f_11_c: null,
  f_11_c_ket: null,
  f_11_d: null,
  f_11_d_ket: null,
  f_11_e: null,
  f_11_e_ket: null,
  f_11_f: null,
  f_11_f_ket: null,
  f_11_g: null,
  f_11_g_ket: null,
  // pembuatan_paspor: null,
  transit: null,
  transit_ket: null,
  error: null,
  loading: null,
  success: null,
  selesai: false,
};

export const PassengerReducer = (state = initialState, action) => {
  if (action.type === 'SET_PASSENGER') {
    return {
      ...state,
      no_paspor: action.payload.no_paspor,
      // kedatangan: action.payload.kedatangan,
      bagasi_dibawa: action.payload.bagasi_dibawa,
      bagasi_tdk_bersama: action.payload.bagasi_tdk_bersama,
      anggota_keluarga: action.payload.anggota_keluarga,
      no_penerbangan: action.payload.no_penerbangan,
      f_11_a: action.payload.f_11_a,
      f_11_a_ket: action.payload.f_11_a_ket,
      f_11_b: action.payload.f_11_b,
      f_11_b_ket: action.payload.f_11_b_ket,
      f_11_c: action.payload.f_11_c,
      f_11_c_ket: action.payload.f_11_c_ket,
      f_11_d: action.payload.f_11_d,
      f_11_d_ket: action.payload.f_11_d_ket,
      f_11_e: action.payload.f_11_e,
      f_11_e_ket: action.payload.f_11_e_ket,
      f_11_f: action.payload.f_11_f,
      f_11_f_ket: action.payload.f_11_f_ket,
      f_11_g: action.payload.f_11_g,
      f_11_g_ket: action.payload.f_11_g_ket,
      // pembuatan_paspor: action.payload.pembuatan_paspor,
      transit: action.payload.transit,
      transit_ket: action.payload.transit_ket,
    };
  }

  if (action.type === 'SET_PASSENGER_LOADING') {
    return {
      ...state,
      loading: action.payload,
    };
  }

  if (action.type === 'SET_PASSENGER_ERROR') {
    return {
      ...state,
      error: action.payload,
    };
  }
  if (action.type === 'SET_PASSENGER_SUCCESS') {
    return {
      ...state,
      success: action.payload,
    };
  }

  if (action.type === 'SET_PASSENGER_DONE') {
    return {
      ...state,
      selesai: action.payload,
    };
  }
  return state;
};
