import {AuthReducer} from './AuthReducer';
import {PassengerReducer} from './PassengerReducer';
import {RegisterReducer} from './RegisterReducer';

export {AuthReducer, PassengerReducer, RegisterReducer};
