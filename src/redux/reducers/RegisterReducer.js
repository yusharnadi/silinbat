const initialState = {
  email: null,
  password: null,
  no_paspor: null,
  no_hp: null,
  name: null,
  pekerjaan: null,
  kebangsaan: null,
  pembuatanPaspor: null,
  alamat_indo: null,
  alamat_malay: null,
  tgl_lahir: null,
  jk: null,
  foto: null,
  loading: false,
  error: null,
  success: null,
};
export const RegisterReducer = (state = initialState, action) => {
  if (action.type === 'SET_REGISTER') {
    return {
      ...state,
      email: action.payload.email,
      password: action.payload.password,
      no_hp: action.payload.no_hp,
      kebangsaan: action.payload.kebangsaan,
      pembuatanPaspor: action.payload.pembuatanPaspor,
      pekerjaan: action.payload.pekerjaan,
      jk: action.payload.jk,
      tgl_lahir: action.payload.tgl_lahir,
      alamat_indo: action.payload.alamat_indo,
      alamat_malay: action.payload.alamat_malay,
      name: action.payload.name,
      no_paspor: action.payload.no_paspor,
    };
  }

  if (action.type === 'GET_REGISTER') {
    return {
      ...state,
      email: action.payload.email,
      password: action.payload.password,
      no_hp: action.payload.no_hp,
      kebangsaan: action.payload.kebangsaan,
      pembuatanPaspor: action.payload.pembuatanPaspor,
      pekerjaan: action.payload.pekerjaan,
      jk: action.payload.jk,
      tgl_lahir: action.payload.tgl_lahir,
      alamat_indo: action.payload.alamat_indo,
      alamat_malay: action.payload.alamat_malay,
      name: action.payload.name,
      no_paspor: action.payload.no_paspor,
      foto: action.payload.foto,
    };
  }

  if (action.type === 'SET_REGISTER_LOADING') {
    return {
      ...state,
      loading: action.payload,
    };
  }

  if (action.type === 'SET_REGISTER_ERROR') {
    return {
      ...state,
      error: action.payload,
    };
  }
  if (action.type === 'SET_REGISTER_SUCCESS') {
    return {
      ...state,
      success: action.payload,
    };
  }

  return state;
};
