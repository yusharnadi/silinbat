import React from 'react';
import {
  Spinner,
  HStack,
  Heading,
  Center,
  NativeBaseProvider,
} from 'native-base';

export default function Loader() {
  return (
    <NativeBaseProvider>
      <HStack
        bgColor="white"
        space={2}
        alignItems="center"
        style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Spinner accessibilityLabel="Loading" color="yellow.500" size="lg" />
        {/* <Heading color="primary.500" fontSize="xl">
          Loading
        </Heading> */}
      </HStack>
    </NativeBaseProvider>
  );
}
