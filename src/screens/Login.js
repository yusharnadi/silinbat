import {NavigationContainer} from '@react-navigation/native';
import {
  NativeBaseProvider,
  Box,
  Center,
  Heading,
  Image,
  Input,
  Button,
  Pressable,
  Text,
  HStack,
  ScrollView,
  FormControl,
} from 'native-base';
import {TouchableOpacity} from 'react-native';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {setAuth} from '../redux/actions';
import Loader from './Loader';
import {Formik} from 'formik';
import * as yup from 'yup';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function Login({navigation}) {
  const [show, setShow] = React.useState(false);
  const RegisterReducer = useSelector(state => state.RegisterReducer);
  const RootState = useSelector(state => state);
  const dispatch = useDispatch();

  const loginValidationSchema = yup.object().shape({
    email: yup
      .string()
      .email('Please enter valid email')
      .required('Email Address is Required'),
    password: yup
      .string()
      .min(6, ({min}) => `Password must be at least ${min} characters`)
      .required('Password is required'),
  });

  const submit = values => {
    console.log(values);
    const form_data = {
      email: values.email,
      password: values.password,
    };
    dispatch(setAuth(form_data));
  };

  if (RegisterReducer.loading) {
    return <Loader />;
  }
  return (
    <NativeBaseProvider>
      <ScrollView>
        <Box flex="1" px={8} pt={4}>
          <Image
            source={require('../../assets/images/logo.png')}
            size="2xl"
            alt="logo beacukai"
            resizeMode="contain"
            alignSelf="center"
          />
          <Heading size="md" alignSelf="center">
            LOGIN
          </Heading>
          {RegisterReducer.error ? (
            <Text fontSize="md" color="red.600" textAlign="center">
              {RegisterReducer.error}
            </Text>
          ) : (
            ''
          )}
          {RootState.AuthReducer.error ? (
            <Text fontSize="md" color="red.600" textAlign="center">
              {RootState.AuthReducer.error}
            </Text>
          ) : (
            ''
          )}
          {/* {RootState.AuthReducer.error} */}

          <Text fontSize="md" color="green.500">
            {RegisterReducer.success}
          </Text>

          <Formik
            validationSchema={loginValidationSchema}
            initialValues={{email: '', password: ''}}
            onSubmit={values => submit(values)}>
            {({handleChange, handleBlur, handleSubmit, errors, values}) => (
              <>
                <FormControl
                  isInvalid={errors.email}
                  w={{
                    base: '100%',
                    md: '100%',
                  }}>
                  {/* <FormControl.Label>Email</FormControl.Label> */}
                  <Input
                    placeholder="Enter Email"
                    size="lg"
                    onChangeText={handleChange('email')}
                    value={values.email}
                    keyboardType="email-address"
                    borderColor="yellow.500"
                  />
                  <FormControl.ErrorMessage>
                    {errors.email}
                  </FormControl.ErrorMessage>
                </FormControl>
                <FormControl
                  isInvalid={errors.password}
                  w={{
                    base: '100%',
                    md: '100%',
                  }}>
                  {/* <FormControl.Label></FormControl.Label> */}
                  <Input
                    mt={4}
                    placeholder="Enter Password"
                    size="lg"
                    onChangeText={handleChange('password')}
                    value={values.password}
                    secureTextEntry={!show}
                    borderColor="yellow.500"
                    InputRightElement={
                      <TouchableOpacity onPress={() => setShow(!show)}>
                        {show ? (
                          <Icon
                            name="eye-slash"
                            size={20}
                            color="gray"
                            style={{marginRight: 10}}
                          />
                        ) : (
                          <Icon
                            name="eye"
                            size={20}
                            color="gray"
                            style={{marginRight: 10}}
                          />
                        )}
                      </TouchableOpacity>
                    }
                  />
                  <FormControl.ErrorMessage>
                    {errors.password}
                  </FormControl.ErrorMessage>
                </FormControl>
                <TouchableOpacity onPress={() => handleSubmit()}>
                  <Box
                    w="100%"
                    rounded="sm"
                    bgColor="yellow.500"
                    alignItems="center"
                    _text={{
                      fontSize: 'md',
                      fontWeight: 'medium',
                      color: 'white',
                    }}
                    p={3}
                    my={4}>
                    LOGIN
                  </Box>
                </TouchableOpacity>
              </>
            )}
          </Formik>

          {/* <Input
              my="1"
              size="lg"
              placeholder="Email"
              borderColor="yellow.500"
              type="email"
              w={{
                base: '100%',
              }}
              onChangeText={e => setEmail(e)}
              keyboardType="email-address"
            />
            <Input
              my="1"
              size="lg"
              borderColor="yellow.500"
              placeholder="Password"
              secureTextEntry
              w={{
                base: '100%',
              }}
              onChangeText={e => setPassword(e)}
            />
            <Button
              onPress={() => submit()}
              size="lg"
              colorScheme="primary"
              bgColor="yellow.500"
              mt="8"
              mb="4"
              w="100%">
              Login
            </Button> */}
          <HStack mt="4" alignItems="center" justifyContent="center" space={1}>
            <Text color="black">Belum Punya Akun ?</Text>
            <Pressable onPress={() => navigation.navigate('Register')}>
              <Text color="yellow.600" fontStyle="italic" fontWeight="bold">
                Daftar
              </Text>
            </Pressable>
          </HStack>
          {/* </Center>*/}
        </Box>
      </ScrollView>
    </NativeBaseProvider>
  );
}
