import React from 'react';
import {
  NativeBaseProvider,
  Box,
  Center,
  Heading,
  TextArea,
  Input,
  Button,
  FormControl,
  ScrollView,
  Select,
  HStack,
} from 'native-base';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment-timezone';
import {useDispatch, useSelector} from 'react-redux';
import {saveRegister} from '../redux/actions';
import Icon from 'react-native-vector-icons/FontAwesome';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {Image, Alert, Platform} from 'react-native';

import FormData from 'form-data';

export default function Biodata({navigation}) {
  const [image, SetImage] = React.useState({
    name: null,
    type: null,
    fileUri: null,
  });
  const [loading, setLoading] = React.useState(false);
  const [date, setDate] = React.useState(new Date());
  const [show, setShow] = React.useState(false);
  const [paspor, setPaspor] = React.useState('');
  const [name, setName] = React.useState('');
  const [kebangsaan, setKebangsaan] = React.useState('');
  const [jk, setJk] = React.useState('');
  const [pekerjaan, setPekerjaan] = React.useState('');
  const [alamatIndo, setIndo] = React.useState('');
  const [alamatMalay, setMalay] = React.useState('');
  const [pembuatanPaspor, setPembuatanPaspor] = React.useState('');
  const [error, setError] = React.useState({
    no_paspor: null,
    name: null,
    pekerjaan: null,
    kebangsaan: null,
    jk: null,
    alamat_indo: null,
    alamat_malay: null,
  });

  console.log(moment.tz.guess());
  console.log(moment(date).tz('Asia/Jakarta').format());

  const RegisterReducer = useSelector(state => state.RegisterReducer);
  const dispatch = useDispatch();

  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const submit = () => {
    if (!image.fileUri) {
      Alert.alert('Foto Profil tidak boleh kosong');
      return;
    }
    if (paspor === '') {
      setError({no_paspor: 'No Paspor tidak boleh kosong.'});
      return;
    }

    if (pembuatanPaspor === '') {
      setError({pembuatanPaspor: 'No Paspor tidak boleh kosong.'});
      return;
    }

    if (name === '') {
      setError({name: 'Nama tidak boleh kosong.'});
      return;
    }
    if (kebangsaan === '') {
      setError({kebangsaan: 'Kebangsaan tidak boleh kosong.'});
      return;
    }

    if (jk === '') {
      setError({jk: 'Jenis Kelamin tidak boleh kosong.'});
      return;
    }

    if (pekerjaan === '') {
      setError({pekerjaan: 'Pekerjaan tidak boleh kosong.'});
      return;
    }
    if (alamatIndo === '') {
      setError({alamat_indo: 'Alamat Indonesia tidak boleh kosong.'});
      return;
    }

    if (alamatMalay === '') {
      setError({alamat_malay: 'Alamat Malaysia tidak boleh kosong.'});
      return;
    }

    setError({
      paspor: null,
      name: null,
      pekerjaan: null,
      kebangsaan: null,
      jk: null,
      alamat_indo: null,
      alamat_malay: null,
      pembuatanPaspor: null,
    });

    const payload = {
      ...RegisterReducer,
      no_paspor: paspor,
      name,
      pekerjaan,
      kebangsaan,
      pembuatanPaspor,
      alamat_indo: alamatIndo,
      alamat_malay: alamatMalay,
      tgl_lahir: moment(date).tz('Asia/Jakarta').format(),
      jk: jk,
    };

    const foto = {
      name: image.name,
      type: image.type,
      uri:
        Platform.OS === 'android'
          ? image.fileUri
          : image.fileUri.replace('file://', ''),
    };

    const form = new FormData();
    form.append('no_paspor', payload.no_paspor);
    form.append('name', payload.name);
    form.append('password', payload.password);
    form.append('email', payload.email);
    form.append('no_hp', payload.no_hp);
    form.append('kebangsaan', payload.kebangsaan);
    form.append('pembuatanPaspor', payload.pembuatanPaspor);
    form.append('tgl_lahir', moment(date).tz('Asia/Jakarta').format());
    form.append('pekerjaan', payload.pekerjaan);
    form.append('alamat_indo', payload.alamat_indo);
    form.append('alamat_malay', payload.alamat_malay);
    form.append('jk', payload.jk);
    form.append('foto', foto);

    dispatch({
      type: 'SET_REGISTER_LOADING',
      payload: true,
    });
    dispatch(saveRegister(form));

    dispatch({type: 'SET_REGISTER', payload});
    navigation.navigate('Login');
  };

  const handleGalery = () => {
    const options = {
      title: 'Pilih Foto',
      saveToPhotos: false,
      maxWidth: 450,
      maxHeight: 600,
      selectionLimit: 1,
      mediaType: 'photo',
    };
    launchImageLibrary(options, function (response) {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        console.log('response', JSON.stringify(response));
        SetImage({
          name: response.assets[0].fileName,
          type: response.assets[0].type,
          fileUri: response.assets[0].uri,
        });
      }
    });
  };

  const handleCamera = () => {
    setLoading(true);
    const options = {
      title: 'Pilih Foto',
      saveToPhotos: false,
      maxWidth: 450,
      maxHeight: 600,
    };

    launchCamera(options, function (response) {
      if (response.didCancel) {
        console.log('User cancelled image picker');

        setLoading(false);
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        Alert.alert('ImagePicker Error: ', response.error);
        setLoading(false);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        setLoading(false);
      } else {
        console.log('response', JSON.stringify(response));
        setLoading(false);

        SetImage({
          name: response.assets[0].fileName,
          type: response.assets[0].type,
          fileUri: response.assets[0].uri,
        });
      }
    });
  };

  return (
    <NativeBaseProvider>
      <ScrollView>
        <Box flex="1">
          <Center px="8">
            <Heading size="xl" mb="2" mt="2">
              Data Diri
            </Heading>
            <Box
              overflow="hidden"
              w="150"
              h="200"
              justifyContent="center"
              alignItems="center">
              <Image
                source={
                  image.fileUri
                    ? {
                        uri: image.fileUri,
                      }
                    : require('../../assets/images/user.png')
                }
                alt="image"
                resizeMode="contain"
                style={{width: 150, height: 200}}
              />
            </Box>
            <HStack space={2} my="2">
              <Button
                bgColor="yellow.500"
                isLoading={loading}
                leftIcon={
                  <Icon
                    name="camera"
                    size={20}
                    color="#fff"
                    onPress={handleCamera}
                  />
                }>
                Camera
              </Button>
              <Button
                bgColor="yellow.500"
                leftIcon={<Icon name="image" size={20} color="#fff" />}
                onPress={handleGalery}>
                Galeri
              </Button>
            </HStack>
            <FormControl
              isInvalid={error.no_paspor}
              w={{
                base: '100%',
                md: '25%',
              }}>
              <FormControl.Label>No Paspor</FormControl.Label>
              <Input
                placeholder="Nomor Paspor"
                size="lg"
                onChangeText={e => setPaspor(e)}
                value={paspor}
                borderColor="yellow.500"
              />
              <FormControl.ErrorMessage>
                {error.no_paspor}
              </FormControl.ErrorMessage>
            </FormControl>
            <FormControl mb={2}>
              <FormControl.Label>Tempat Penerbitan Paspor</FormControl.Label>
              <Input
                size="lg"
                placeholder="Tempat Penerbitan Paspor"
                borderColor="yellow.500"
                // _focus={{borderColor: color.primary}}
                // color={color.primary}
                value={pembuatanPaspor}
                onChangeText={e => setPembuatanPaspor(e)}
              />
            </FormControl>
            <FormControl
              isInvalid={error.name}
              w={{
                base: '100%',
                md: '25%',
              }}>
              <FormControl.Label>Nama Lengkap</FormControl.Label>
              <Input
                placeholder="Nama Lengkap"
                size="lg"
                onChangeText={e => setName(e)}
                value={name}
                borderColor="yellow.500"
              />
              <FormControl.ErrorMessage>{error.name}</FormControl.ErrorMessage>
            </FormControl>
            <FormControl isInvalid={error.kebangsaan}>
              <FormControl.Label>Kebangsaan</FormControl.Label>
              <Select
                borderColor="yellow.500"
                selectedValue={kebangsaan}
                onValueChange={itemValue => setKebangsaan(itemValue)}
                minWidth="200"
                accessibilityLabel="Kebangsaan"
                placeholder="Kebangsaan"
                _selectedItem={{
                  bg: 'teal.400',
                  // endIcon: <CheckIcon size={5} />,
                }}>
                <Select.Item label="Indonesia" value="Indonesia" />
                <Select.Item label="Malaysia" value="Malaysia" />
                <Select.Item label="Brunei" value="Brunei" />
                <Select.Item label="Singapura" value="Singapura" />
                <Select.Item label="Thailand" value="Thailand" />
                <Select.Item label="Filipina" value="Filipina" />
              </Select>
              <FormControl.ErrorMessage>
                {error.kebangsaan}
              </FormControl.ErrorMessage>
              <FormControl isInvalid={error.jk}>
                <FormControl.Label>Jenis Kelamin</FormControl.Label>
                <Select
                  borderColor="yellow.500"
                  selectedValue={jk}
                  onValueChange={itemValue => setJk(itemValue)}
                  minWidth="200"
                  accessibilityLabel="Jenis Kelamin"
                  placeholder="Jenis Kelamin"
                  _selectedItem={{
                    bg: 'teal.400',
                    // endIcon: <CheckIcon size={5} />,
                  }}>
                  <Select.Item label="Laki-Laki" value="Laki-Laki" />
                  <Select.Item label="Perempuan" value="Perempuan" />
                </Select>
                <FormControl.ErrorMessage>{error.jk}</FormControl.ErrorMessage>
              </FormControl>
            </FormControl>
            <FormControl
              isInvalid={error.pekerjaan}
              w={{
                base: '100%',
                md: '25%',
              }}>
              <FormControl.Label>Pekerjaan</FormControl.Label>
              <Input
                placeholder="Pekerjaan"
                size="lg"
                onChangeText={e => setPekerjaan(e)}
                value={pekerjaan}
                borderColor="yellow.500"
              />
              <FormControl.ErrorMessage>
                {error.pekerjaan}
              </FormControl.ErrorMessage>
            </FormControl>
            <FormControl
              w={{
                base: '100%',
                md: '25%',
              }}>
              <FormControl.Label>Tanggal Lahir</FormControl.Label>
              <Input
                size="lg"
                borderColor="yellow.500"
                placeholder=""
                onFocus={() => setShow(true)}
                value={moment(date).format('D MMMM YYYY')}
                InputRightElement={
                  <Icon
                    name="calendar"
                    size={20}
                    color="#000"
                    onPress={() => setShow(true)}
                    style={{marginRight: 10}}
                  />
                }
              />
            </FormControl>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode="date"
                display="default"
                onChange={onChangeDate}
              />
            )}
            <FormControl isInvalid={error.alamat_indo}>
              <FormControl.Label>Alamat di Indonesia</FormControl.Label>
              <TextArea
                value={alamatIndo}
                onChangeText={e => setIndo(e)}
                placeholder="Alamat Indonesia"
                w={{
                  base: '100%',
                  md: '25%',
                }}
                borderColor="yellow.500"
              />
              <FormControl.ErrorMessage>
                {error.alamat_indo}
              </FormControl.ErrorMessage>
            </FormControl>
            <FormControl isInvalid={error.alamat_malay}>
              <FormControl.Label>Alamat di Malaysia</FormControl.Label>
              <TextArea
                borderColor="yellow.500"
                value={alamatMalay}
                onChangeText={e => setMalay(e)}
                placeholder="Alamat Malaysia"
                w={{
                  base: '100%',
                  md: '25%',
                }}
              />
              <FormControl.ErrorMessage>
                {error.alamat_malay}
              </FormControl.ErrorMessage>
            </FormControl>
            <Button
              onPress={() => submit()}
              size="lg"
              w="100%"
              bgColor="yellow.500"
              colorScheme="primary"
              my="8">
              Daftar
            </Button>
          </Center>
        </Box>
      </ScrollView>
    </NativeBaseProvider>
  );
}
