import {Box, NativeBaseProvider, Button, HStack} from 'native-base';
import React from 'react';
import {Platform, Image} from 'react-native';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome';
import FormData from 'form-data';
import axios from 'axios';
import {Api} from '../config/Api';
import {useSelector} from 'react-redux';
import Loader from './Loader';

export default function FotoUpdate({navigation, route}) {
  const [loading, setLoading] = React.useState(false);
  const [loader, setLoader] = React.useState(false);
  const [disable, setDisable] = React.useState(true);
  const [image, SetImage] = React.useState({
    name: null,
    type: null,
    fileUri: null,
  });
  const AuthReducer = useSelector(state => state.AuthReducer);

  const handleGalery = () => {
    const options = {
      title: 'Pilih Foto',
      saveToPhotos: false,
      maxWidth: 450,
      maxHeight: 600,
      selectionLimit: 1,
      mediaType: 'photo',
    };
    launchImageLibrary(options, function (response) {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        console.log('response', JSON.stringify(response));
        SetImage({
          name: response.assets[0].fileName,
          type: response.assets[0].type,
          fileUri: response.assets[0].uri,
        });
        setDisable(false);
      }
    });
  };

  const handleCamera = () => {
    setLoading(true);
    const options = {
      title: 'Pilih Foto',
      saveToPhotos: false,
      maxWidth: 450,
      maxHeight: 600,
    };

    launchCamera(options, function (response) {
      if (response.didCancel) {
        console.log('User cancelled image picker');

        setLoading(false);
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        setLoading(false);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        setLoading(false);
      } else {
        console.log('response', JSON.stringify(response));
        setLoading(false);

        SetImage({
          name: response.assets[0].fileName,
          type: response.assets[0].type,
          fileUri: response.assets[0].uri,
        });
        setDisable(false);
      }
    });
  };

  const handleSubmit = () => {
    setLoader(true);
    const foto = {
      name: image.name,
      type: image.type,
      uri:
        Platform.OS === 'android'
          ? image.fileUri
          : image.fileUri.replace('file://', ''),
    };

    const form = new FormData();

    form.append('foto', foto);
    axios
      .post(`${Api}/personal/foto/${AuthReducer.no_paspor}`, form, {
        headers: {
          Authorization: 'Bearer ' + AuthReducer.token,
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(response => {
        console.log(response.data);
        setLoader(false);
        navigation.navigate('Profile');
      })
      .catch(err => console.log(err.response));
  };

  if (loader) return <Loader />;

  return (
    <NativeBaseProvider>
      <Box p={8} alignItems="center">
        <Image
          source={
            image.fileUri
              ? {
                  uri: image.fileUri,
                }
              : {
                  uri: route.params.foto,
                }
          }
          alt="image"
          resizeMode="contain"
          style={{width: 200, height: 300}}
        />
        <HStack space={2} my="2">
          <Button
            bgColor="yellow.500"
            isLoading={loading}
            leftIcon={
              <Icon
                name="camera"
                size={20}
                color="#fff"
                onPress={handleCamera}
              />
            }>
            Camera
          </Button>
          <Button
            bgColor="yellow.500"
            leftIcon={<Icon name="image" size={20} color="#fff" />}
            onPress={handleGalery}>
            Galeri
          </Button>
        </HStack>
      </Box>
      <Box p={8} flex={1} justifyContent="flex-end">
        <Button
          bgColor="blue.900"
          isDisabled={disable}
          size="lg"
          onPress={handleSubmit}>
          Simpan
        </Button>
      </Box>
    </NativeBaseProvider>
  );
}
