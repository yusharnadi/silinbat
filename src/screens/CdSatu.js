import {
  NativeBaseProvider,
  FormControl,
  Input,
  Box,
  ScrollView,
} from 'native-base';
import React from 'react';
import {Platform, Alert, TouchableOpacity} from 'react-native';
import color from '../config/color';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment-timezone';
import {useSelector, useDispatch} from 'react-redux';
import HeadingForm from '../components/HeadingForm';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function CdSatu({navigation}) {
  const AuthReducer = useSelector(state => state.AuthReducer);
  // const [date, setDate] = React.useState(new Date());
  // const [show, setShow] = React.useState(false);
  const [bagasi_dibawa, setBagasiBawa] = React.useState('');
  const [bagasi_tdk_bersama, setBagasiTdkBawa] = React.useState('');
  const [anggota_keluarga, setKeluarga] = React.useState('');
  const [no_penerbangan, setPenerbangan] = React.useState('');

  // console.log(moment.tz.guess());
  // console.log(moment(date).tz('Asia/Jakarta').format());

  // const onChangeDate = (event, selectedDate) => {
  //   const currentDate = selectedDate || date;
  //   setShow(Platform.OS === 'ios');
  //   setDate(currentDate);
  // };

  const dispatch = useDispatch();
  const onSubmit = () => {
    if (bagasi_dibawa === '') {
      Alert.alert('Form Tidak Boleh Kosong.  ');
      return;
    }
    if (bagasi_tdk_bersama === '') {
      Alert.alert('Form Tidak Boleh Kosong.  ');
      return;
    }
    if (anggota_keluarga === '') {
      Alert.alert('Form Tidak Boleh Kosong.  ');
      return;
    }

    const payload = {
      // kedatangan: moment(date).tz('Asia/Jakarta').format(),
      bagasi_dibawa,
      bagasi_tdk_bersama,
      anggota_keluarga,
      no_penerbangan,
      no_paspor: AuthReducer.no_paspor,
    };

    dispatch({type: 'SET_PASSENGER', payload});
    // console.log('DISPATCHING ...');
    // console.log(passenger);
    navigation.navigate('CdDua');
  };

  return (
    <NativeBaseProvider>
      <ScrollView>
        <HeadingForm title="Passenger" subTitle="Customs Declaration" />
        <Box px="8">
          {/* <FormControl mb={2}>
            <FormControl.Label
              _text={{
                color: color.primary,
                fontSize: 'md',
                fontWeight: 400,
              }}>
              Kedatangan
            </FormControl.Label>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode="date"
                display="default"
                onChange={onChangeDate}
              />
            )}

            <Input
              size="lg"
              borderColor={color.primary}
              _focus={{borderColor: color.primary}}
              color={color.primary}
              onFocus={() => setShow(true)}
              value={moment(date).format('D MMMM YYYY')}
              InputRightElement={
                <Icon
                  onPress={() => setShow(true)}
                  name="calendar"
                  size={20}
                  color="#000"
                  style={{marginRight: 20}}
                />
              }
            />
          </FormControl> */}
          {/* <FormControl mb={2}>
            <FormControl.Label
              _text={{
                color: color.primary,
                fontSize: 'md',
                fontWeight: 400,
              }}>
              Tempat Penerbitan Paspor
            </FormControl.Label>
            <Input
              size="lg"
              placeholder=""
              borderColor={color.primary}
              _focus={{borderColor: color.primary}}
              color={color.primary}
              value={pembuatan_paspor}
              onChangeText={e => setPembuatanPaspor(e)}
            />
          </FormControl> */}
          <FormControl mb={2}>
            <FormControl.Label
              _text={{
                color: color.primary,
                fontSize: 'md',
                fontWeight: 400,
              }}>
              Jumlah Bagasi yang Dibawa
            </FormControl.Label>
            <Input
              size="lg"
              placeholder="0"
              borderColor={color.primary}
              _focus={{borderColor: color.primary}}
              color={color.primary}
              value={bagasi_dibawa}
              keyboardType="numeric"
              onChangeText={e => setBagasiBawa(e)}
            />
          </FormControl>
          <FormControl mb={2}>
            <FormControl.Label
              _text={{
                color: color.primary,
                fontSize: 'md',
                fontWeight: 400,
              }}>
              Jumlah Bagasi yang datang tidak bersamaan
            </FormControl.Label>
            <Input
              size="lg"
              placeholder="0"
              borderColor={color.primary}
              _focus={{borderColor: color.primary}}
              color={color.primary}
              value={bagasi_tdk_bersama}
              keyboardType="numeric"
              onChangeText={e => setBagasiTdkBawa(e)}
            />
          </FormControl>
          <FormControl mb={2}>
            <FormControl.Label
              _text={{
                color: color.primary,
                fontSize: 'md',
                fontWeight: 400,
              }}>
              Jumlah Anggota Keluarga yang berpergian bersama
            </FormControl.Label>
            <Input
              size="lg"
              placeholder="0"
              borderColor={color.primary}
              _focus={{borderColor: color.primary}}
              color={color.primary}
              value={anggota_keluarga}
              keyboardType="numeric"
              onChangeText={e => setKeluarga(e)}
            />
          </FormControl>
          <FormControl mb={2}>
            <FormControl.Label
              _text={{
                color: color.primary,
                fontSize: 'md',
                fontWeight: 400,
              }}>
              No Penerbangan / Pelayaran / Keberangkatan
            </FormControl.Label>
            <Input
              size="lg"
              placeholder="0"
              borderColor={color.primary}
              _focus={{borderColor: color.primary}}
              color={color.primary}
              value={no_penerbangan}
              onChangeText={e => setPenerbangan(e)}
            />
          </FormControl>

          <TouchableOpacity onPress={() => onSubmit()}>
            <Box
              w="100%"
              rounded="sm"
              bgColor="yellow.500"
              alignItems="center"
              _text={{
                fontSize: 'md',
                fontWeight: 'medium',
                color: 'white',
              }}
              p={3}
              my={4}>
              Selanjutnya
            </Box>
          </TouchableOpacity>
        </Box>
      </ScrollView>
    </NativeBaseProvider>
  );
}
