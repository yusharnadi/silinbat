import axios from 'axios';
import {NativeBaseProvider, Box, HStack, Text, ScrollView} from 'native-base';
import React from 'react';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useSelector, useDispatch} from 'react-redux';
import {Api} from '../config/Api';
import Loader from './Loader';
import moment from 'moment-timezone';
import {getLogout} from '../redux/actions';

export default function History({navigation}) {
  const [passenger, setPassenger] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const AuthReducer = useSelector(state => state.AuthReducer);
  const dispatch = useDispatch();

  const fetch = () => {
    setLoading(true);
    axios
      .get(`${Api}/passenger/history`, {
        headers: {Authorization: 'Bearer ' + AuthReducer.token},
      })
      .then(result => {
        setPassenger(result.data.data);
        setLoading(false);
      })
      .catch(err => {
        if (err.response.status === 401) {
          console.log('Logout Call from Histori');
          dispatch(getLogout(AuthReducer.token));
          setLoading(false);
          return;
        }
        setLoading(false);
        console.log(err.response);
      });
  };

  React.useEffect(() => {
    fetch();
  }, []);

  if (loading) return <Loader />;
  return (
    <NativeBaseProvider>
      <Box flex={1} px={8} py={2}>
        <ScrollView>
          {passenger?.map(item => {
            return (
              <TouchableOpacity
                key={item.id_passenger}
                onPress={() => {
                  navigation.navigate('HistoryDetail', {
                    id: item.id_passenger,
                  });
                }}>
                <Box
                  py="2"
                  px="4"
                  mt="4"
                  bgColor="yellow.500"
                  rounded="lg"
                  overflow="hidden">
                  <HStack alignItems="center">
                    <Box flex={1}>
                      <Text fontSize={16} fontWeight={800} color="white">
                        {item.kode_passenger}
                      </Text>
                      <Text color="white" fontStyle="italic">
                        {'PLBN ' + item.plbn} |{' '}
                        {moment(item.kedatangan).format('D MMMM YYYY')}
                      </Text>
                    </Box>
                    <Box>
                      <Icon name="arrow-right" size={20} color="white" />
                    </Box>
                  </HStack>
                </Box>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </Box>
    </NativeBaseProvider>
  );
}
