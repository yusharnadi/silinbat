import React from 'react';
import {Image, VStack, Heading, Button, NativeBaseProvider} from 'native-base';
import color from '../config/color';
import {useSelector, useDispatch} from 'react-redux';
import Loader from './Loader';

export default function CdDone({navigation}) {
  const dispatch = useDispatch();
  const PassengerReducer = useSelector(state => state.PassengerReducer);
  // const passenger = useSelector(state => state.PassengerReducer);

  // console.log(PassengerReducer);
  const Sukses = () => (
    <>
      <Heading color={color.primary} fontSize="4xl" textAlign="center">
        Berhasil
      </Heading>
      <Image
        source={require('../../assets/images/success.png')}
        style={{height: 250}}
        resizeMode="contain"
        alt="success"
      />
      <Heading color={color.primary} fontSize="md" textAlign="center">
        Terima Kasih, Customs Declaration anda telah masuk ke sistem.
      </Heading>
    </>
  );

  const Gagal = () => (
    <>
      <Heading color="red.600" fontSize="4xl" textAlign="center">
        Gagal
      </Heading>
      <Image
        source={require('../../assets/images/gagal.png')}
        style={{height: 250}}
        resizeMode="contain"
        alt="success"
      />
      <Heading color="red.600" fontSize="md" textAlign="center">
        Gagal menyimpan data ke server, silahkan coba lagi.
      </Heading>
    </>
  );

  if (PassengerReducer.loading) {
    return <Loader />;
  }

  const handleHome = () => {
    navigation.navigate('Home');
    dispatch({type: 'SET_PASSENGER_SUCCESS', payload: null});
  };

  return (
    <NativeBaseProvider>
      <VStack
        p="4"
        bgColor="white"
        space={2}
        alignItems="center"
        style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        {PassengerReducer.success ? <Sukses /> : <Gagal />}
        <Button
          onPress={() => handleHome()}
          size="lg"
          colorScheme="primary"
          bgColor={color.primary}
          mt="8">
          Beranda
        </Button>
      </VStack>
    </NativeBaseProvider>
  );
}
