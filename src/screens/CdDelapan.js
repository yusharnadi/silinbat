import {
  NativeBaseProvider,
  Box,
  Radio,
  Text,
  TextArea,
  ScrollView,
} from 'native-base';
import React from 'react';
import HeadingForm from '../components/HeadingForm';
import {useDispatch, useSelector} from 'react-redux';
import color from '../config/color';
import {TouchableOpacity} from 'react-native';

export default function CdDelapan({navigation}) {
  const passenger = useSelector(state => state.PassengerReducer);
  // const AuthReducer = useSelector(state => state.AuthReducer);
  const [option, setOption] = React.useState(null);
  const [textArea, setTextArea] = React.useState('');

  const dispatch = useDispatch();

  const payload = {
    ...passenger,
    f_11_g: option,
    f_11_g_ket: textArea,
  };

  const onSubmit = () => {
    dispatch({type: 'SET_PASSENGER', payload});
    navigation.navigate('CdSembilan');
  };

  return (
    <NativeBaseProvider>
      <ScrollView>
        <HeadingForm title="Passenger" subTitle="Customs Declaration" />
        <Box px="8">
          <Radio.Group
            defaultValue="1"
            name="myRadioGroup"
            accessibilityLabel="Pick your favorite number"
            value={option}
            onChange={nextValue => {
              setOption(nextValue);
            }}>
            <Text color={color.primary} fontWeight="600" fontSize="md">
              Apakah anda membawa Barang impor yang akan digunakan untuk tujuan
              selain pemakaian pribadi (jumlah tidak wajar utk dikonsumsi
              sendiri atau untuk keperluan perusahaan/toko/institusi/industri) ?
            </Text>
            <Radio value="Ya" my={1}>
              Ya
            </Radio>
            <Radio value="Tidak" my={1}>
              Tidak
            </Radio>
          </Radio.Group>
          <TextArea
            placeholder="Silahkan isi keterangan jika pilihan anda sebelumnya ( Ya )"
            value={textArea}
            onChangeText={e => setTextArea(e)}
          />
          <TouchableOpacity onPress={() => onSubmit()}>
            <Box
              w="100%"
              rounded="sm"
              bgColor="yellow.500"
              alignItems="center"
              _text={{
                fontSize: 'md',
                fontWeight: 'medium',
                color: 'white',
              }}
              p={3}
              my={4}>
              Selanjutnya
            </Box>
          </TouchableOpacity>
        </Box>
      </ScrollView>
    </NativeBaseProvider>
  );
}
