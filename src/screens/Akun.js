import React from 'react';
import {TouchableOpacity} from 'react-native';
import {Formik} from 'formik';
import * as yup from 'yup';
import {Box, NativeBaseProvider, FormControl, Input, Button} from 'native-base';
import axios from 'axios';
import {Api} from '../config/Api';
import {useSelector} from 'react-redux';
import Loader from './Loader';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function Akun({navigation, route}) {
  const {email, no_hp} = route.params.profile;
  const [loading, setLoading] = React.useState(false);
  const [show, setShow] = React.useState(false);

  const AuthReducer = useSelector(state => state.AuthReducer);

  const loginValidationSchema = yup.object().shape({
    email: yup
      .string()
      .email('Please enter valid email')
      .required('Email Address is Required'),
    password: yup
      .string()
      .min(6, ({min}) => `Password must be at least ${min} characters`)
      .required('Password is required'),
  });

  const submit = values => {
    setLoading(true);
    axios
      .post(`${Api}/personal/akun/`, values, {
        headers: {
          Authorization: 'Bearer ' + AuthReducer.token,
          Accept: 'application/json',
        },
      })
      .then(response => {
        console.log(response.data);
        setLoading(false);
        navigation.navigate('Profile');
      })
      .catch(err => console.log(err.response));
  };

  if (loading) return <Loader />;
  return (
    <NativeBaseProvider>
      <Box px={8} py={6}>
        <Formik
          validationSchema={loginValidationSchema}
          initialValues={{email, password: '', no_hp}}
          onSubmit={values => submit(values)}>
          {({handleChange, handleBlur, handleSubmit, errors, values}) => (
            <>
              <FormControl
                isInvalid={errors.email}
                w={{
                  base: '100%',
                  md: '25%',
                }}>
                <FormControl.Label>Email</FormControl.Label>
                <Input
                  isDisabled={true}
                  placeholder="Enter Email"
                  size="lg"
                  onChangeText={handleChange('email')}
                  value={values.email}
                  keyboardType="email-address"
                  borderColor="gray.500"
                />
                <FormControl.ErrorMessage>
                  {errors.email}
                </FormControl.ErrorMessage>
              </FormControl>
              <FormControl
                isInvalid={errors.password}
                w={{
                  base: '100%',
                  md: '25%',
                }}>
                <FormControl.Label>Password</FormControl.Label>
                <Input
                  placeholder="Enter Password"
                  size="lg"
                  onChangeText={handleChange('password')}
                  value={values.password}
                  secureTextEntry={!show}
                  borderColor="yellow.500"
                  InputRightElement={
                    <TouchableOpacity onPress={() => setShow(!show)}>
                      {show ? (
                        <Icon
                          name="eye-slash"
                          size={20}
                          color="gray"
                          style={{marginRight: 10}}
                        />
                      ) : (
                        <Icon
                          name="eye"
                          size={20}
                          color="gray"
                          style={{marginRight: 10}}
                        />
                      )}
                    </TouchableOpacity>
                  }
                />
                <FormControl.ErrorMessage>
                  {errors.password}
                </FormControl.ErrorMessage>
              </FormControl>
              <Button
                onPress={() => handleSubmit()}
                size="lg"
                w="100%"
                bgColor="yellow.500"
                colorScheme="primary"
                mt="16">
                Simpan
              </Button>
            </>
          )}
        </Formik>
      </Box>
    </NativeBaseProvider>
  );
}
