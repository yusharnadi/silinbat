import {
  NativeBaseProvider,
  Box,
  Center,
  Text,
  Flex,
  Container,
} from 'native-base';
import React from 'react';
import {View} from 'react-native';
import QRCode from 'react-native-qrcode-svg';

export default function GenerateQR({navigation, route}) {
  const kode_passenger = route.params.kode_passenger;
  console.log(kode_passenger);
  return (
    <NativeBaseProvider>
      <Box p="8" flex={1}>
        <Center>
          <Text fontSize={16} textAlign="center">
            Silahkan Tunjukan QRCODE ini Ke petugas
          </Text>
        </Center>
        <Flex alignItems="center" mt="16">
          <QRCode value={kode_passenger.toString()} size={230} />
        </Flex>
      </Box>
    </NativeBaseProvider>
  );
}
