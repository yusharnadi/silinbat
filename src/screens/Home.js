import {
  Box,
  HStack,
  NativeBaseProvider,
  Image,
  Center,
  Text,
  ArrowForwardIcon,
  Heading,
} from 'native-base';
import React from 'react';
import {TouchableOpacity} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
import {useDispatch, useSelector} from 'react-redux';
import color from '../config/color';
import {getLogout} from '../redux/actions';
import axios from 'axios';
import {Api} from '../config/Api';

export default function Home({navigation, route}) {
  const [slider, setSlider] = React.useState([]);
  // const slider = [
  //   // 'https://source.unsplash.com/1024x768/?tree', // Network image
  //   require('../../assets/images/aruk-new.png'), // Local image
  //  // require('../../assets/images/badau-new.png'), // Local image
  //   //require('../../assets/images/entikong.jpeg'), // Local image
  //   // require('../../assets/images/aruk.jpeg'),
  //   // require('../../assets/images/badau.jpeg'),
  // ];

  const getRemoteSlider = () => {
    axios
      .get(`${Api}/slider`)
      .then(response => {
        let temp = [];
        response.data.data?.map(item => {
          temp.push(item.image_path);
        });
        setSlider(temp);
      })
      .catch(err => console.log(err));
  };

  React.useEffect(() => {
    getRemoteSlider();
  }, []);

  const user_data = useSelector(state => state.AuthReducer);
  const shortName = user_data?.name?.split(' ')[0].toUpperCase();

  const dispatch = useDispatch();

  const logout = () => {
    console.log('Logout Call');
    dispatch(getLogout(user_data.token));
  };

  return (
    <NativeBaseProvider>
      <Box flex={1} bgColor="yellow.500">
        <Box h="320" p="6">
          <HStack alignItems="center" justifyContent="space-between">
            <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
              <HStack alignItems="center">
                <Image
                  source={require('../../assets/images/avatar.png')}
                  alt="asda"
                  w={10}
                  h={10}
                  borderRadius="xl"
                  mr={2}
                  resizeMode="contain"
                />
                <Text color="white" fontSize="md">
                  {shortName}
                </Text>
              </HStack>
            </TouchableOpacity>
            <HStack>
              <Text color="white" fontSize="md" mr={2} onPress={() => logout()}>
                Logout
              </Text>
              <ArrowForwardIcon size="6" color="white" />
            </HStack>
          </HStack>
          <Box
            bgColor="red"
            height={220}
            mt={4}
            borderRadius="md"
            overflow="hidden">
            <SliderBox
              images={slider}
              autoplay
              circleLoop
              sliderBoxHeight={220}
            />
          </Box>
        </Box>
        <Box flex={1} bgColor="white" borderTopRadius={16} p="8">
          <HStack justifyContent="space-between">
            <TouchableOpacity onPress={() => navigation.navigate('CdSatu')}>
              <Box alignItems="center">
                <Center bgColor={color.primary} h="20" w="20" borderRadius="xl">
                  <Image
                    source={require('../../assets/images/passanger.png')}
                    alt="asda"
                  />
                </Center>
                <Heading size="xs" mt="1">
                  PASSENGER
                </Heading>
              </Box>
            </TouchableOpacity>
            <Box alignItems="center">
              <Center bgColor={color.primary} h="20" w="20" borderRadius="xl">
                <Image
                  source={require('../../assets/images/borang.png')}
                  alt="asda"
                />
              </Center>
              <Heading size="xs" mt="1">
                BORANG
              </Heading>
            </Box>
            <Box alignItems="center">
              <Center bgColor={color.primary} h="20" w="20" borderRadius="xl">
                <Image
                  source={require('../../assets/images/kilb.png')}
                  alt="asda"
                />
              </Center>
              <Heading size="xs" mt="1">
                KILB
              </Heading>
            </Box>
          </HStack>
          <HStack justifyContent="space-between" mt="6">
            <TouchableOpacity onPress={() => navigation.navigate('Checkout')}>
              <Box alignItems="center">
                <Center bgColor={color.primary} h="20" w="20" borderRadius="xl">
                  <Image
                    source={require('../../assets/images/checkout.png')}
                    alt="asda"
                  />
                </Center>
                <Heading size="xs" mt="1">
                  CHECKOUT
                </Heading>
              </Box>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('History')}>
              <Box alignItems="center">
                <Center bgColor={color.primary} h="20" w="20" borderRadius="xl">
                  <Image
                    source={require('../../assets/images/riwayat.png')}
                    alt="asda"
                  />
                </Center>
                <Heading size="xs" mt="1">
                  RIWAYAT
                </Heading>
              </Box>
            </TouchableOpacity>
            <Box alignItems="center">
              <Center bgColor={color.primary} h="20" w="20" borderRadius="xl">
                <Image
                  source={require('../../assets/images/bantuan.png')}
                  alt="asda"
                />
              </Center>
              <Heading size="xs" mt="1">
                BANTUAN
              </Heading>
            </Box>
          </HStack>
        </Box>
      </Box>
    </NativeBaseProvider>
  );
}
