import React from 'react';
import {
  NativeBaseProvider,
  Box,
  Center,
  Heading,
  TextArea,
  Input,
  Button,
  FormControl,
  Select,
  HStack,
} from 'native-base';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment-timezone';
import {useSelector} from 'react-redux';
import {ScrollView, Platform} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Loader from './Loader';
import axios from 'axios';
import {Api} from '../config/Api';

export default function Personal({navigation, route}) {
  const params = route.params.profile;
  const [loading, setLoading] = React.useState(false);
  const [date, setDate] = React.useState(new Date(params.tgl_lahir));
  const [show, setShow] = React.useState(false);
  const [paspor, setPaspor] = React.useState(params.no_paspor);
  const [name, setName] = React.useState(params.nama);
  const [kebangsaan, setKebangsaan] = React.useState(params.kebangsaan);
  const [pembuatanPaspor, setPembuatanPaspor] = React.useState(
    params.pembuatanPaspor,
  );
  const [jk, setJk] = React.useState(params.jk);
  const [pekerjaan, setPekerjaan] = React.useState(params.pekerjaan);
  const [alamatIndo, setIndo] = React.useState(params.alamat_indo);
  const [alamatMalay, setMalay] = React.useState(params.alamat_malay);
  const [hp, setHp] = React.useState(params.no_hp);

  const [error, setError] = React.useState({
    no_paspor: null,
    name: null,
    pekerjaan: null,
    kebangsaan: null,
    pembuatanPaspor: null,
    jk: null,
    alamat_indo: null,
    alamat_malay: null,
    hp: null,
  });

  const AuthReducer = useSelector(state => state.AuthReducer);

  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const submit = () => {
    if (paspor === '') {
      setError({no_paspor: 'No Paspor tidak boleh kosong.'});
      return;
    }

    if (pembuatanPaspor === '') {
      setError({
        pembuatanPaspor: 'Tempat Penerbitan Paspor tidak boleh kosong.',
      });
      return;
    }

    if (name === '') {
      setError({name: 'Nama tidak boleh kosong.'});
      return;
    }
    if (kebangsaan === '') {
      setError({kebangsaan: 'Kebangsaan tidak boleh kosong.'});
      return;
    }

    if (jk === '') {
      setError({jk: 'Jenis Kelamin tidak boleh kosong.'});
      return;
    }
    if (hp === '') {
      setError({hp: 'No HP tidak boleh kosong.'});
      return;
    }

    if (pekerjaan === '') {
      setError({pekerjaan: 'Pekerjaan tidak boleh kosong.'});
      return;
    }
    if (alamatIndo === '') {
      setError({alamat_indo: 'Alamat Indonesia tidak boleh kosong.'});
      return;
    }

    if (alamatMalay === '') {
      setError({alamat_malay: 'Alamat Malaysia tidak boleh kosong.'});
      return;
    }

    setError({
      paspor: null,
      name: null,
      pekerjaan: null,
      kebangsaan: null,
      pembuatanPaspor: null,
      jk: null,
      alamat_indo: null,
      alamat_malay: null,
      hp: null,
    });

    const payload = {
      name,
      pekerjaan,
      kebangsaan,
      pembuatanPaspor,
      no_hp: hp,
      alamat_indo: alamatIndo,
      alamat_malay: alamatMalay,
      tgl_lahir: moment(date).tz('Asia/Jakarta').format('YYYY-MM-DD'),
      jk: jk,
    };

    setLoading(true);
    axios
      .post(`${Api}/personal/person/`, payload, {
        headers: {
          Authorization: 'Bearer ' + AuthReducer.token,
          Accept: 'application/json',
        },
      })
      .then(response => {
        // console.log(response.data);
        setLoading(false);
        navigation.navigate('Profile');
      })
      .catch(err => console.log(err.response));
  };

  if (loading) return <Loader />;

  return (
    <NativeBaseProvider>
      <ScrollView>
        <Box flex="1">
          <Center px="8">
            <FormControl
              isDisabled={true}
              mt={4}
              w={{
                base: '100%',
                md: '25%',
              }}>
              <FormControl.Label>No Paspor</FormControl.Label>
              <Input
                placeholder="Nomor Paspor"
                size="lg"
                onChangeText={e => setPaspor(e)}
                value={paspor}
                borderColor="gray.500"
              />
              <FormControl.ErrorMessage>
                {error.no_paspor}
              </FormControl.ErrorMessage>
            </FormControl>
            <FormControl mb={2}>
              <FormControl.Label>Tempat Penerbitan Paspor</FormControl.Label>
              <Input
                size="lg"
                placeholder="Tempat Penerbitan Paspor"
                borderColor="yellow.500"
                // _focus={{borderColor: color.primary}}
                // color={color.primary}
                value={pembuatanPaspor}
                onChangeText={e => setPembuatanPaspor(e)}
              />
            </FormControl>
            <FormControl
              isInvalid={error.name}
              w={{
                base: '100%',
                md: '25%',
              }}>
              <FormControl.Label>Nama Lengkap</FormControl.Label>
              <Input
                placeholder="Nama Lengkap"
                size="lg"
                onChangeText={e => setName(e)}
                value={name}
                borderColor="yellow.500"
              />
              <FormControl.ErrorMessage>{error.name}</FormControl.ErrorMessage>
            </FormControl>
            <FormControl
              isInvalid={error.hp}
              w={{
                base: '100%',
                md: '25%',
              }}>
              <FormControl.Label>No Handphone</FormControl.Label>
              <Input
                placeholder="No Handphone"
                size="lg"
                onChangeText={e => setHp(e)}
                value={hp}
                borderColor="yellow.500"
              />
              <FormControl.ErrorMessage>{error.name}</FormControl.ErrorMessage>
            </FormControl>
            <FormControl isInvalid={error.kebangsaan}>
              <FormControl.Label>Kebangsaan</FormControl.Label>
              <Select
                borderColor="yellow.500"
                selectedValue={kebangsaan}
                defaultValue={kebangsaan}
                onValueChange={itemValue => setKebangsaan(itemValue)}
                minWidth="200"
                accessibilityLabel="Kebangsaan"
                placeholder="Kebangsaan"
                _selectedItem={{
                  bg: 'teal.400',
                  // endIcon: <CheckIcon size={5} />,
                }}>
                <Select.Item label="Indonesia" value="Indonesia" />
                <Select.Item label="Malaysia" value="Malaysia" />
                <Select.Item label="Brunei" value="Brunei" />
                <Select.Item label="Singapura" value="Singapura" />
                <Select.Item label="Thailand" value="Thailand" />
                <Select.Item label="Filipina" value="Filipina" />
              </Select>
              <FormControl.ErrorMessage>
                {error.kebangsaan}
              </FormControl.ErrorMessage>
              <FormControl isInvalid={error.jk}>
                <FormControl.Label>Jenis Kelamin</FormControl.Label>
                <Select
                  borderColor="yellow.500"
                  selectedValue={jk}
                  defaultValue={jk}
                  onValueChange={itemValue => setJk(itemValue)}
                  minWidth="200"
                  accessibilityLabel="Jenis Kelamin"
                  placeholder="Jenis Kelamin"
                  _selectedItem={{
                    bg: 'teal.400',
                    // endIcon: <CheckIcon size={5} />,
                  }}>
                  <Select.Item label="Laki-Laki" value="Laki-Laki" />
                  <Select.Item label="Perempuan" value="Perempuan" />
                </Select>
                <FormControl.ErrorMessage>{error.jk}</FormControl.ErrorMessage>
              </FormControl>
            </FormControl>
            <FormControl
              isInvalid={error.pekerjaan}
              w={{
                base: '100%',
                md: '25%',
              }}>
              <FormControl.Label>Pekerjaan</FormControl.Label>
              <Input
                placeholder="Pekerjaan"
                size="lg"
                onChangeText={e => setPekerjaan(e)}
                value={pekerjaan}
                borderColor="yellow.500"
              />
              <FormControl.ErrorMessage>
                {error.pekerjaan}
              </FormControl.ErrorMessage>
            </FormControl>
            <FormControl
              w={{
                base: '100%',
                md: '25%',
              }}>
              <FormControl.Label>Tanggal Lahir</FormControl.Label>
              <Input
                size="lg"
                borderColor="yellow.500"
                placeholder=""
                onFocus={() => setShow(true)}
                value={moment(date).format('D MMMM YYYY')}
                InputRightElement={
                  <Icon
                    name="calendar"
                    size={20}
                    color="#000"
                    onPress={() => setShow(true)}
                    style={{marginRight: 10}}
                  />
                }
              />
            </FormControl>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode="date"
                display="default"
                onChange={onChangeDate}
              />
            )}
            <FormControl isInvalid={error.alamat_indo}>
              <FormControl.Label>Alamat di Indonesia</FormControl.Label>
              <TextArea
                value={alamatIndo}
                onChangeText={e => setIndo(e)}
                placeholder="Alamat Indonesia"
                w={{
                  base: '100%',
                  md: '25%',
                }}
                borderColor="yellow.500"
              />
              <FormControl.ErrorMessage>
                {error.alamat_indo}
              </FormControl.ErrorMessage>
            </FormControl>
            <FormControl isInvalid={error.alamat_malay}>
              <FormControl.Label>Alamat di Malaysia</FormControl.Label>
              <TextArea
                borderColor="yellow.500"
                value={alamatMalay}
                onChangeText={e => setMalay(e)}
                placeholder="Alamat Malaysia"
                w={{
                  base: '100%',
                  md: '25%',
                }}
              />
              <FormControl.ErrorMessage>
                {error.alamat_malay}
              </FormControl.ErrorMessage>
            </FormControl>
            <Button
              onPress={() => submit()}
              size="lg"
              w="100%"
              bgColor="yellow.500"
              colorScheme="primary"
              my="8">
              Simpan
            </Button>
          </Center>
        </Box>
      </ScrollView>
    </NativeBaseProvider>
  );
}
