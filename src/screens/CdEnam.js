import {
  NativeBaseProvider,
  Box,
  Button,
  Radio,
  Text,
  TextArea,
  ScrollView,
} from 'native-base';
import React from 'react';
import HeadingForm from '../components/HeadingForm';
import {useDispatch, useSelector} from 'react-redux';
import color from '../config/color';
import {Alert, TouchableOpacity} from 'react-native';

export default function CdEnam({navigation}) {
  const passenger = useSelector(state => state.PassengerReducer);
  const [option, setOption] = React.useState(null);
  const [textArea, setTextArea] = React.useState('');

  const dispatch = useDispatch();

  const payload = {
    ...passenger,
    f_11_e: option,
    f_11_e_ket: textArea,
  };

  const onSubmit = () => {
    if (option === null) {
      Alert.alert('Form Tidak Boleh Kosong.  ');
      return;
    }
    dispatch({type: 'SET_PASSENGER', payload});
    console.log('DISPATCHING  step 6...');
    navigation.navigate('CdTujuh');
  };

  //   console.log(passenger);

  return (
    <NativeBaseProvider>
      <ScrollView>
        <HeadingForm title="Passenger" subTitle="Customs Declaration" />
        <Box px="8">
          <Radio.Group
            defaultValue="1"
            name="myRadioGroup"
            accessibilityLabel="Pick your favorite number"
            value={option}
            onChange={nextValue => {
              setOption(nextValue);
            }}>
            <Text color={color.primary} fontWeight="600" fontSize="md">
              Apakah anda membawa Lebih dari 200 batang sigaret, 25 batang
              cerutu, atau 100 gram tembakau iris / produk hasil tembakau
              lainnya dan / atau 1 liter minuman mengandung etil alkohol (utk
              Penumpang) ATAU 40 btg sigaret, 10 btg cerutu atau 40 gr tembakau
              iris/ hasil tembakau lainnya dan / atau 350 mililiter minuman
              mengandung alkohol ?
            </Text>
            <Radio value="Ya" my={1}>
              Ya
            </Radio>
            <Radio value="Tidak" my={1}>
              Tidak
            </Radio>
          </Radio.Group>
          <TextArea
            placeholder="Silahkan isi keterangan jika pilihan anda sebelumnya ( Ya )"
            value={textArea}
            onChangeText={e => setTextArea(e)}
          />
          <TouchableOpacity onPress={() => onSubmit()}>
            <Box
              w="100%"
              rounded="sm"
              bgColor="yellow.500"
              alignItems="center"
              _text={{
                fontSize: 'md',
                fontWeight: 'medium',
                color: 'white',
              }}
              p={3}
              my={4}>
              Selanjutnya
            </Box>
          </TouchableOpacity>
        </Box>
      </ScrollView>
    </NativeBaseProvider>
  );
}
