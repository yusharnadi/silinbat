import axios from 'axios';
import {
  NativeBaseProvider,
  Box,
  Avatar,
  Text,
  ScrollView,
  Button,
} from 'native-base';
import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {Api} from '../config/Api';
import Loader from './Loader';
import moment from 'moment-timezone';
import {deletePassenger} from '../redux/actions';

export default function CheckoutDetail({navigation, route}) {
  const id = route.params.id;
  const [passenger, setPassenger] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const AuthReducer = useSelector(state => state.AuthReducer);

  const dispatch = useDispatch();
  const fetch = () => {
    setLoading(true);
    axios
      .get(`${Api}/passenger/detail/${id}`, {
        headers: {Authorization: 'Bearer ' + AuthReducer.token},
      })
      .then(result => {
        console.log(result.data);
        setPassenger(result.data.data);
        setLoading(false);
      })
      .catch(err => {
        setLoading(false);
        console.log(err.response);
      });
  };

  const handleDelete = ids => {
    // setLoading(true);
    dispatch(deletePassenger(ids, AuthReducer.token));

    navigation.navigate('Home');
  };

  React.useEffect(() => {
    fetch();
  }, []);

  if (loading) return <Loader />;
  return (
    <NativeBaseProvider safeArea>
      <Box flex={1} px={8}>
        {/* <HeadingForm title="CUSTOMS DECLARATIONS" subTitle="Passenger" /> */}

        <ScrollView showsVerticalScrollIndicator={false}>
          <Avatar
            mt={4}
            bg="gray.400"
            alignSelf="center"
            size="2xl"
            source={{
              uri: passenger.foto_full,
            }}>
            A
          </Avatar>
          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Kode CD :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.kode_passenger}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              No Paspor :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.no_paspor}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Nama Lengkap :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.nama}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Kebangsaan :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.kebangsaan}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Tanggal Lahir :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {moment(passenger.tgl_lahir).format('D MMMM YYYY')}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Jenis Kelamin :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.jk}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Pekerjaan :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.pekerjaan}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Alamat Indonesia :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.alamat_indo}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Alamat Malaysia :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.alamat_malay}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Kedatangan :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {moment(passenger.kedatangan).format('D MMMM YYYY')}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Jumlah Bagasi yang dibawa :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.bagasi_dibawa}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Jumlah Bagasi Tidak Bersama :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.bagasi_tdk_bersama}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Jumlah Anggota Keluarga yang berpergian Bersama :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.anggota_keluarga}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              No Penerbangan / Keberangkatan :
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.no_penerbangan}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              A. Apakah anda membawa Hewan, ikan dan tumbuhan termasuk produk
              yang berasal dari hewan, ikan dan tumbuhan ?
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.f_11_a} , {passenger.f_11_a_ket}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              B. Apakah anda membawa Narkotika, psikotropika, prekursor,
              obat-obatan, senjata api, senjata angin, senjata tajam, amunisi,
              bahan peledak, benda / publikasi pornografi ?
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.f_11_b} , {passenger.f_11_b_ket}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              C. Apakah anda membawa Uang atau instrumen pembayaran lain dalam
              rupiah atau dalam mata uang asing senilai dengan Rp. 100.000.000 ?
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.f_11_c} , {passenger.f_11_c_ket}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              D. Apakah anda membawa Uang Kertas Asing paling sedikit setara
              dengan Rp. 1.000.000.000 (1 milyar rupiah) ?
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.f_11_d} , {passenger.f_11_d_ket}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              E. Apakah anda membawa Lebih dari 200 batang sigaret, 25 batang
              cerutu, atau 100 gram tembakau iris / produk hasil tembakau
              lainnya dan / atau 1 liter minuman mengandung etil alkohol (utk
              Penumpang) ATAU 40 btg sigaret, 10 btg cerutu atau 40 gr tembakau
              iris/ hasil tembakau lainnya dan / atau 350 mililiter minuman
              mengandung alkohol ?
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.f_11_e} , {passenger.f_11_e_ket}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              F. Apakah anda membawa Barang utk keperluan pribadi yang dibeli
              diluar negeri dan tidak dibawa kembali ke luar negeri dengan nilai
              melebih USD 50.00 per orang(untuk awak pengankut) atau USD 500.00
              per orang (untuk penumpang) ?
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.f_11_f} , {passenger.f_11_f_ket}
            </Text>
          </Box>

          <Box
            py="2"
            px="4"
            mt="2"
            bgColor="yellow.500"
            rounded="lg"
            overflow="hidden">
            <Text color="white" fontStyle="italic">
              Apakah anda membawa Barang impor yang akan digunakan untuk tujuan
              selain pemakaian pribadi (jumlah tidak wajar utk dikonsumsi
              sendiri atau untuk keperluan perusahaan/toko/institusi/industri) ?
            </Text>
            <Text fontSize={16} fontWeight={800} color="white">
              {passenger.f_11_g} , {passenger.f_11_g_ket}
            </Text>
          </Box>
          <Button
            onPress={() =>
              navigation.navigate('QRCode', {
                kode_passenger: passenger.id_passenger,
              })
            }
            size="lg"
            colorScheme="primary"
            bgColor="blue.700"
            mt="8">
            Tampilkan QR Code
          </Button>
          <TouchableOpacity
            onPress={() => handleDelete(passenger.id_passenger)}>
            <Box
              rounded="sm"
              bgColor="red.500"
              alignItems="center"
              _text={{
                fontSize: 'md',
                fontWeight: 'medium',
                color: 'white',
              }}
              p={2}
              mb="4"
              mt={2}>
              Batalkan / Hapus
            </Box>
          </TouchableOpacity>
        </ScrollView>
      </Box>
    </NativeBaseProvider>
  );
}
