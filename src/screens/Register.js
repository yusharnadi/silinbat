/* eslint-disable react/self-closing-comp */
import {
  NativeBaseProvider,
  Box,
  Center,
  Heading,
  Image,
  Input,
  Button,
  Pressable,
  Text,
  FormControl,
  ScrollView,
} from 'native-base';
import React from 'react';
import {TouchableOpacity} from 'react-native';
import {Formik} from 'formik';
import * as yup from 'yup';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function Register({navigation}) {
  const [show, setShow] = React.useState(false);
  const loginValidationSchema = yup.object().shape({
    email: yup
      .string()
      .email('Please enter valid email')
      .required('Email Address is Required'),
    password: yup
      .string()
      .min(6, ({min}) => `Password must be at least ${min} characters`)
      .required('Password is required'),
    no_hp: yup.string().required('Phone Number is required'),
  });
  const RegisterReducer = useSelector(state => state.RegisterReducer);
  const dispatch = useDispatch();
  const submit = values => {
    dispatch({type: 'SET_REGISTER', payload: values});
    console.log(RegisterReducer);
    navigation.navigate('Biodata');
  };
  return (
    <NativeBaseProvider>
      <ScrollView>
        <Box flex="1">
          <Center p="8">
            <Image
              source={require('../../assets/images/logo.png')}
              size="xl"
              alt="logo beacukai"
              resizeMode="contain"
            />
            <Heading size="md" mb="2">
              REGISTRASI AKUN
            </Heading>
            <Formik
              validationSchema={loginValidationSchema}
              initialValues={{email: '', password: '', no_hp: ''}}
              onSubmit={values => submit(values)}>
              {({handleChange, handleBlur, handleSubmit, errors, values}) => (
                <>
                  <FormControl
                    isInvalid={errors.no_hp}
                    w={{
                      base: '100%',
                      md: '25%',
                    }}>
                    <FormControl.Label>No Handphone</FormControl.Label>
                    <Input
                      placeholder="Enter Phone Number"
                      size="lg"
                      onChangeText={handleChange('no_hp')}
                      value={values.no_hp}
                      keyboardType="numeric"
                      borderColor="yellow.500"
                    />
                    <FormControl.ErrorMessage>
                      {errors.no_hp}
                    </FormControl.ErrorMessage>
                  </FormControl>
                  <FormControl
                    isInvalid={errors.email}
                    w={{
                      base: '100%',
                      md: '25%',
                    }}>
                    <FormControl.Label>Email</FormControl.Label>
                    <Input
                      placeholder="Enter Email"
                      size="lg"
                      onChangeText={handleChange('email')}
                      value={values.email}
                      keyboardType="email-address"
                      borderColor="yellow.500"
                    />
                    <FormControl.ErrorMessage>
                      {errors.email}
                    </FormControl.ErrorMessage>
                  </FormControl>
                  <FormControl
                    isInvalid={errors.password}
                    w={{
                      base: '100%',
                      md: '25%',
                    }}>
                    <FormControl.Label>Password</FormControl.Label>
                    <Input
                      placeholder="Enter Password"
                      size="lg"
                      onChangeText={handleChange('password')}
                      value={values.password}
                      secureTextEntry={!show}
                      borderColor="yellow.500"
                      InputRightElement={
                        <TouchableOpacity onPress={() => setShow(!show)}>
                          {show ? (
                            <Icon
                              name="eye-slash"
                              size={20}
                              color="gray"
                              style={{marginRight: 10}}
                            />
                          ) : (
                            <Icon
                              name="eye"
                              size={20}
                              color="gray"
                              style={{marginRight: 10}}
                            />
                          )}
                        </TouchableOpacity>
                      }
                    />
                    <FormControl.ErrorMessage>
                      {errors.password}
                    </FormControl.ErrorMessage>
                  </FormControl>
                  <Button
                    onPress={() => handleSubmit()}
                    size="lg"
                    w="100%"
                    bgColor="yellow.500"
                    colorScheme="primary"
                    mt="8">
                    Daftar
                  </Button>
                </>
              )}
            </Formik>
            <Pressable mt="16" onPress={() => navigation.navigate('Login')}>
              <Text color="primary.600">Sudah Punya Akun ? Login disini</Text>
            </Pressable>
          </Center>
        </Box>
      </ScrollView>
    </NativeBaseProvider>
  );
}
