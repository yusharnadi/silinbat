import React from 'react';
import {
  NativeBaseProvider,
  Box,
  Radio,
  Text,
  TextArea,
  ScrollView,
} from 'native-base';
import HeadingForm from '../components/HeadingForm';
import {useDispatch, useSelector} from 'react-redux';
import color from '../config/color';
import {TouchableOpacity} from 'react-native';

export default function CdSembilan({navigation}) {
  const passenger = useSelector(state => state.PassengerReducer);
  // const AuthReducer = useSelector(state => state.AuthReducer);
  const [option, setOption] = React.useState(null);
  const [textArea, setTextArea] = React.useState('');

  const dispatch = useDispatch();

  const payload = {
    ...passenger,
    transit: option,
    transit_ket: textArea,
  };

  const onSubmit = () => {
    dispatch({type: 'SET_PASSENGER', payload});
    navigation.navigate('Sign');
  };
  return (
    <NativeBaseProvider>
      <ScrollView>
        <HeadingForm title="Passenger" subTitle="Customs Declaration" />
        <Box px="8">
          <Radio.Group
            defaultValue="1"
            name="myRadioGroup"
            accessibilityLabel="Pick your favorite number"
            value={option}
            onChange={nextValue => {
              setOption(nextValue);
            }}>
            <Text color={color.primary} fontWeight="600" fontSize="md">
              Apakah anda transit dari plbn atau negara lain sebelum sampai ke
              sini ?
            </Text>
            <Radio value="Ya" my={1}>
              Ya
            </Radio>
            <Radio value="Tidak" my={1}>
              Tidak
            </Radio>
          </Radio.Group>
          <TextArea
            placeholder="Silahkan isi keterangan lokasi / negara transit  jika pilihan anda sebelumnya ( Ya )"
            value={textArea}
            onChangeText={e => setTextArea(e)}
          />
          <TouchableOpacity onPress={() => onSubmit()}>
            <Box
              w="100%"
              rounded="sm"
              bgColor="yellow.500"
              alignItems="center"
              _text={{
                fontSize: 'md',
                fontWeight: 'medium',
                color: 'white',
              }}
              p={3}
              my={4}>
              Selanjutnya
            </Box>
          </TouchableOpacity>
        </Box>
      </ScrollView>
    </NativeBaseProvider>
  );
}
