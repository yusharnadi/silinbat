import {
  Button,
  NativeBaseProvider,
  HStack,
  Text,
  Container,
  Flex,
} from 'native-base';
import React from 'react';
import {View, Alert} from 'react-native';
import Signature from 'react-native-signature-canvas';
import {useSelector, useDispatch} from 'react-redux';
import {savePassenger} from '../redux/actions/Passenger';
import Loader from './Loader';

export default function Sign({text, onOK, navigation}) {
  const [signImage, setSignImage] = React.useState(null);

  const passenger = useSelector(state => state.PassengerReducer);
  const AuthReducer = useSelector(state => state.AuthReducer);
  const dispatch = useDispatch();
  const ref = React.useRef();
  console.log(passenger);

  React.useEffect(() => {
    return () => {
      dispatch({
        type: 'SET_PASSENGER_DONE',
        payload: false,
      });
    };
  }, []);

  // Called after ref.current.readSignature() reads a non-empty base64 string
  const handleOK = signature => {
    setSignImage(signature);
  };

  // Called after ref.current.readSignature() reads an empty string
  const handleEmpty = () => {
    console.log('Empty');
    Alert.alert('Tanda tangan Kosong.');
  };

  // Called after ref.current.clearSignature()
  const handleClear = () => {
    ref.current.clearSignature();
    setSignImage(null);
    // console.log('clear success!');
  };

  // Called after end of stroke
  const handleEnd = () => {
    ref.current.readSignature();
  };

  // Called after ref.current.getData()
  const handleData = data => {
    console.log(data);
  };

  const handleSubmit = () => {
    if (signImage === null) {
      Alert.alert('Tanda Tangan tidak boleh Kosong.');
      return;
    }
    const paramData = {
      ...passenger,
      sign: signImage,
    };
    dispatch(savePassenger(paramData, AuthReducer.token));
  };

  if (passenger.loading) {
    return <Loader />;
  }

  if (passenger.selesai) {
    navigation.navigate('CdDone');
  }

  const imgWidth = 300;
  const imgHeight = 200;
  const styler = `.m-signature-pad {border-width: 2px;} 
              .m-signature-pad--body {border: none;}
              .m-signature-pad--footer {display: none; margin: 0px;}
              body,html {
                width: ${imgWidth}px; height: ${imgHeight}px;}
              `;
  return (
    <View style={{flex: 1}}>
      <NativeBaseProvider>
        <Flex
          my={8}
          h={imgHeight}
          w={imgWidth}
          alignItems="center"
          justifyContent="center"
          alignSelf="center">
          <Signature
            webStyle={styler}
            ref={ref}
            onEnd={handleEnd}
            onOK={handleOK}
            onEmpty={handleEmpty}
            onGetData={handleData}
            autoClear={false}
            descriptionText="Silahkan Tanda Tangan di atas."
          />
        </Flex>
        <Container px={6} py={2}>
          <Text fontWeight="bold">PERHATIAN !</Text>
          <Text>Tanda tangan sebaiknya memenuhi Kotak Putih di atas. </Text>
        </Container>
        <HStack space={2} justifyContent="center">
          <Button mt={4} size="lg" onPress={handleClear} bgColor="gray.400">
            Hapus
          </Button>
          <Button mt={4} size="lg" onPress={handleSubmit} bgColor="yellow.500">
            Simpan
          </Button>
        </HStack>
      </NativeBaseProvider>
    </View>
  );
}

const style = `
                .m-signature-pad--body {border-width: 2px;}
              .m-signature-pad--footer {display: none; margin: 0px;}
              
              `;
