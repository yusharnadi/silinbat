/* eslint-disable react-hooks/exhaustive-deps */
import axios from 'axios';
import {
  Avatar,
  Box,
  HStack,
  VStack,
  NativeBaseProvider,
  Text,
  Heading,
} from 'native-base';
import React from 'react';
import {TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Api} from '../config/Api';
import {useSelector, useDispatch} from 'react-redux';
import {getLogout} from '../redux/actions';
import Loader from './Loader';

export default function Profile({navigation}) {
  const [profile, setProfile] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  const AuthReducer = useSelector(state => state.AuthReducer);
  const dispatch = useDispatch();

  const fetchData = () => {
    axios
      .get(`${Api}/profile`, {
        headers: {Authorization: 'Bearer ' + AuthReducer.token},
      })
      .then(result => {
        console.log(result.data);
        setProfile(result.data.data);
        setLoading(false);
      })
      .catch(err => {
        if (err.response.status === 401) {
          console.log('Logout Call from Histori');
          dispatch(getLogout(AuthReducer.token));
          setLoading(false);
          return;
        }
        console.log(err.response);
        setLoading(false);
      });
  };

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      fetchData();
    });
    return unsubscribe;
  }, [navigation]);

  const logout = () => {
    console.log('Logout Call');
    dispatch(getLogout(AuthReducer.token));
  };

  if (loading) return <Loader />;

  return (
    <NativeBaseProvider>
      <Box bgColor="gray.100" flex={1}>
        <HStack space={4} alignItems="center" px={6} py={4} bgColor="white">
          {/* <Avatar
            size="lg"
            source={{
              uri: profile.foto,
            }}
          /> */}
          <Image
            source={{
              uri: profile?.foto,
            }}
            alt="asda"
            style={{height: 60, width: 60, borderRadius: 60}}
            resizeMode="cover"
          />
          <Box overflow="hidden" flex={1}>
            <Text fontSize={20} fontWeight="semibold" color="gray.600">
              {profile.nama}
            </Text>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('UpdateFoto', {foto: profile.foto})
              }>
              <Text color="yellow.600">
                Ubah Foto <Icon name="angle-right" />
              </Text>
            </TouchableOpacity>
          </Box>
        </HStack>
        <VStack space={4} px={6} py={4} bgColor="white" mt={4}>
          <Heading color="yellow.500" size="sm">
            Pengaturan
          </Heading>
          <TouchableOpacity>
            <Text
              color="gray.600"
              fontSize={16}
              onPress={() => navigation.navigate('UpdateAkun', {profile})}>
              Akun
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('Personal', {profile})}>
            <Text color="gray.600" fontSize={16}>
              Data Pribadi
            </Text>
          </TouchableOpacity>
        </VStack>
        <VStack space={4} px={6} py={4} bgColor="white" mt={4}>
          <Heading color="yellow.500" size="sm">
            Umum
          </Heading>
          <Text color="gray.600" fontSize={16}>
            Faq's
          </Text>
          <Text color="gray.600" fontSize={16}>
            Privacy and Policy
          </Text>
          <Text color="gray.600" fontSize={16}>
            Disclaimers
          </Text>
        </VStack>
        <VStack space={4} px={6} py={4} bgColor="white" mt={4}>
          <TouchableOpacity onPress={logout}>
            <Text color="gray.600" fontSize={16} fontWeight="bold">
              Logout
            </Text>
          </TouchableOpacity>
        </VStack>
      </Box>
    </NativeBaseProvider>
  );
}
