import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator();
import Register from './screens/Register';
import Login from './screens/Login';
import Biodata from './screens/Biodata';
import Home from './screens/Home';
import CdSatu from './screens/CdSatu';
import CdDua from './screens/CdDua';
import CdTiga from './screens/CdTiga';
import CdEmpat from './screens/CdEmpat';
import CdLima from './screens/CdLima';
import {useSelector, useDispatch} from 'react-redux';
import {getAuth} from './redux/actions';
import Loader from './screens/Loader';
import CdEnam from './screens/CdEnam';
import CdTujuh from './screens/CdTujuh';
import CdDelapan from './screens/CdDelapan';
import CdDone from './screens/CdDone';
import Sign from './screens/Sign';
import GenerateQR from './screens/GenerateQR';
import Checkout from './screens/Checkout';
import CheckoutDetail from './screens/CheckoutDetail';
import History from './screens/History';
import HistoryDetail from './screens/HistoryDetail';
import Profile from './screens/Profile';
import FotoUpdate from './screens/FotoUpdate';
import Akun from './screens/Akun';
import Personal from './screens/Personal';
import CdSembilan from './screens/CdSembilan';

export default function Router() {
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(getAuth());
  }, [dispatch]);
  const RootState = useSelector(state => state.AuthReducer);

  if (RootState.loading) {
    return <Loader />;
  }
  const RootStack = () => {
    return (
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          headerStyle: {
            backgroundColor: '#eab308',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: '400',
          },
        }}
        initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen
          name="CdSatu"
          component={CdSatu}
          options={{
            headerShown: true,
            title: 'Step 1/9',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="CdDua"
          component={CdDua}
          options={{
            headerShown: true,
            title: 'Step 2/9',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="CdTiga"
          component={CdTiga}
          options={{
            headerShown: true,
            title: 'Step 3/9',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="CdEmpat"
          component={CdEmpat}
          options={{
            headerShown: true,
            title: 'Step 4/9',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="CdLima"
          component={CdLima}
          options={{
            headerShown: true,
            title: 'Step 5/9',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="CdEnam"
          component={CdEnam}
          options={{
            headerShown: true,
            title: 'Step 6/9',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="CdTujuh"
          component={CdTujuh}
          options={{
            headerShown: true,
            title: 'Step 7/9',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="CdDelapan"
          component={CdDelapan}
          options={{
            headerShown: true,
            title: 'Step 8/9',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="CdSembilan"
          component={CdSembilan}
          options={{
            headerShown: true,
            title: 'Step 9/9',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen name="CdDone" component={CdDone} />
        <Stack.Screen
          name="Sign"
          component={Sign}
          options={{
            headerShown: true,
            title: 'Tanda Tangan',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="Checkout"
          component={Checkout}
          options={{
            headerShown: true,
            title: 'Checkout',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="CheckoutDetail"
          component={CheckoutDetail}
          options={{
            headerShown: true,
            title: 'Detail CD',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="QRCode"
          component={GenerateQR}
          options={{
            headerShown: true,
            title: 'Scan QR CODE',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="History"
          component={History}
          options={{
            headerShown: true,
            title: 'Riwayat',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="HistoryDetail"
          component={HistoryDetail}
          options={{
            headerShown: true,
            title: 'Riwayat Detail',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={{
            headerShown: true,
            title: 'Profil',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="UpdateFoto"
          component={FotoUpdate}
          options={{
            headerShown: true,
            title: 'Ganti Foto ',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="UpdateAkun"
          component={Akun}
          options={{
            headerShown: true,
            title: 'Edit Akun ',
            headerBackVisible: true,
          }}
        />
        <Stack.Screen
          name="Personal"
          component={Personal}
          options={{
            headerShown: true,
            title: 'Edit Data ',
            headerBackVisible: true,
          }}
        />
      </Stack.Navigator>
    );
  };

  const AuthStack = () => {
    return (
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          headerStyle: {
            backgroundColor: '#eab308',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: '400',
          },
        }}
        initialRouteName="Login">
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Biodata" component={Biodata} />
      </Stack.Navigator>
    );
  };
  return (
    <NavigationContainer>
      {RootState.token === null ? <AuthStack /> : <RootStack />}
    </NavigationContainer>
  );
}
