import React from 'react';
import {Container, Heading, Text} from 'native-base';
import color from '../config/color';

export default function HeadingForm({title, subTitle}) {
  return (
    <Container p="8">
      <Text>{title}</Text>
      <Heading color={color.primary}>{subTitle}</Heading>
    </Container>
  );
}
