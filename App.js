import React from 'react';

import SplashScreen from 'react-native-splash-screen';
import {Provider} from 'react-redux';
import Router from './src/Router';
import store from './src/redux/store';

export default function App() {
  React.useEffect(() => {
    SplashScreen.hide();
  });

  return (
    <Provider store={store}>
      <Router />
    </Provider>
  );
}
